import React, {Component} from 'react';
import {
  View,
  Text,
  StyleSheet,
  Image,
  TouchableOpacity,
  Alert,
  TextInput,
} from 'react-native';
import {Input} from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome';
import {Sae} from 'react-native-textinput-effects';
import FontAwesomeIcon from 'react-native-vector-icons/FontAwesome';
import {color} from 'react-native-reanimated';

class LoginView extends Component {
  constructor(props) {
    super(props);
    this.state = {
      textUser: '',
      textPassword: '',
      count: 0,
      icon: 'eye-slash',
      password: true,
      data: {
        user: 'george',
        password: '123456',
      },
    };
  }

  changeTextInput = text => {
    this.setState({textUser: text});
  };
  textPassword(text) {
    this.setState({textPassword: text});
  }
  changeIcon() {
    this.setState(prevState => ({
      icon: prevState.icon === 'eye' ? 'eye-slash' : 'eye',
      password: !prevState.password,
    }));
  }
  onLogin() {
    this.state.textUser === this.state.data.user &&
    this.state.textPassword === this.state.data.password
      ? this.props.navigation.reset({
          index: 0,
          routes: [
            {
              name: 'Home',
            },
          ],
        })
      : Alert.alert('Error', 'Try again');
    console.log(this.state.textPassword, this.state.data.password);
  }
  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.title}> Login </Text>
        <Image
          source={{
            uri:
              'https://www.pngkey.com/png/full/432-4327458_peliculas-icono-png-film.png',
          }}
          style={{
            width: 200,
            height: 200,
            alignSelf: 'center',
          }}
        />

        <Input
          inputContainerStyle={styles.TextInput}
          placeholder="username"
          placeholderTextColor="gray"
          onChangeText={text => this.changeTextInput(text)}
          inputStyle={styles.writeInput}
          value={this.state.textUser}
          leftIcon={<Icon name="user" size={24} color="white" />}
        />

        <Input
          onChangeText={this.textPassword.bind(this)}
          inputContainerStyle={styles.TextInput}
          inputStyle={styles.writeInput}
          value={this.state.textPassword}
          secureTextEntry={this.state.password}
          placeholder="password"
          placeholderTextColor="gray"
          leftIcon={<Icon name="lock" size={24} color="white" />}
          rightIcon={
            <Icon
              name={this.state.icon}
              size={24}
              color={'white'}
              onPress={this.changeIcon.bind(this)}
            />
          }
        />
        <TouchableOpacity
          onPress={this.onLogin.bind(this)}
          style={[
            this.state.textUser.length > 0 && this.state.textPassword.length > 0
              ? styles.buttonFinal
              : styles.buttonInitial,
          ]}>
          <Text style={styles.textButton}>Iniciar sesión</Text>
        </TouchableOpacity>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    backgroundColor: '#131313',
    flex: 1,
    alignSelf: 'stretch',
    padding: 20,
  },
  TextInput: {
    backgroundColor: '#262626',
    paddingRight: 20,
    paddingLeft: 10,
    borderBottomWidth: 0,
    borderRadius: 10,
    marginRight: -10,
    marginLeft: -10,
    marginTop: 20,
    padding: 5,
  },
  writeInput: {
    paddingStart: 15,
    color: 'white',
  },
  buttonInitial: {
    marginTop: 20,
    borderWidth: 1.5,
    borderColor: 'white',
    padding: 15,
    borderRadius: 10,
  },
  buttonFinal: {
    borderWidth: 1.5,
    borderColor: 'red',
    backgroundColor: 'red',
    marginTop: 20,
    padding: 15,
    borderRadius: 10,
  },
  textButton: {
    color: 'white',
    alignSelf: 'center',
    fontSize: 20,
  },
});
export default LoginView;
