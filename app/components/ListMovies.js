import React, {Component} from 'react';
import {
  View,
  Text,
  StyleSheet,
  Image,
  FlatList,
  Alert,
  TouchableOpacity,
} from 'react-native';
import Icon from 'react-native-vector-icons/dist/FontAwesome';

export default class ListMovies extends Component {
  constructor(props) {
    super(props);

    this.state = {
      textValue: 0,
      count: 0,
      items: [],
      error: null,
    };
  }

  renderItem = ({item}) => (
    <View style={styles.container}>
      <TouchableOpacity
        onPress={() => {
          this.props.navigation.navigate('DetailsMovie', {
            itemId: item.id,
          });
        }}>
        <View style={styles.itemList}>
          <View>
            <Image
              source={{
                uri: item.medium_cover_image,
              }}
              style={styles.imageItem}
            />
          </View>
          <View style={styles.descrition}>
            <Text numberOfLines={1} style={styles.titleItem}>
              {item.title}
            </Text>
            <Text numberOfLines={3} style={styles.subtitleItem}>
              {item.synopsis}
            </Text>
          </View>
          <Icon size={30} name="chevron-right" color={'white'} />
        </View>
      </TouchableOpacity>
    </View>
  );

  async componentDidMount() {
    await fetch('https://yts.mx/api/v2/list_movies.json')
      .then(res => res.json())
      .then(
        result => {
          console.warn('result', result.data.movies);
          this.setState({
            items: result.data.movies,
          });
        },
        error => {
          this.setState({
            error: error,
          });
        },
      );
  }

  render() {
    return (
      <View>
        <FlatList
          data={this.state.items.length > 0 ? this.state.items : []}
          /* renderItem={({item}) => (
            <Item title={item.title} image={item.small_cover_image} />
          )} */
          renderItem={this.renderItem}
          keyExtractor={item => item.id}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'black',
    paddingRight: 10,
    paddingLeft: 10,
  },
  itemList: {
    flex: 1,
    flexDirection: 'row',
    /* backgroundColor: 'red', */
    borderBottomWidth: 1,
    alignItems: 'center',
    padding: 20,
    backgroundColor: 'black',
    borderBottomWidth: 1.2,
    borderBottomColor: 'white',
    borderRadius: 25,
  },
  imageItem: {
    width: 80,
    height: 80,
    borderRadius: 100,
  },
  descrition: {
    flex: 1,
    alignItems: 'stretch',
    paddingLeft: 20,
    paddingRight: 20,
  },
  titleItem: {
    /* backgroundColor: 'blue', */
    alignSelf: 'center',
    fontWeight: 'bold',
    fontSize: 20,
    color: 'white',
  },
  subtitleItem: {
    /* backgroundColor: 'green', */
    color: 'gray',
    paddingTop: 10,
    textAlign: 'justify',
    fontFamily: 'arial',
  },
});
